<?php
namespace App\Model;
use PDO;
use PDOException;

class Database{
    public $DBH;
    public $host="localhost";
    public $dbname="atomic_project_b3x";
    public $user="root";
    public $password="";

    public function __construct()
    {
        try
        {
            $this->DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user,$this->password);
            echo "Successfully Connected DB<br >";
        }
        catch(PDOException $error){
            echo $error->getMessage();
        }

    }
}